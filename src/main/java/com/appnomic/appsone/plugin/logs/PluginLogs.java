package com.appnomic.appsone.plugin.logs;

import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.appnomic.appsone.common.protbuf.NotificationProtos;
import com.appnomic.appsone.eventhandler.lib.exception.PluginException;
import com.appnomic.appsone.eventhandler.lib.plugin.Plugin;
import com.appnomic.appsone.eventhandler.lib.plugin.PluginContext;
import com.appnomic.appsone.eventhandler.lib.plugin.PluginEvent;
import com.appnomic.appsone.eventhandler.lib.plugin.PluginStats;
import com.google.protobuf.InvalidProtocolBufferException;

public class PluginLogs implements Plugin {

	private static Logger logger = LoggerFactory.getLogger(PluginLogs.class);
	public static String dedupKeyPrefix = "";
	

	public void init(PluginContext context) throws PluginException {
		logger.info("logger Plugin Initilization started");
		Map<String, String> contextArgs = context.getArgs();
		if(contextArgs!=null && !contextArgs.isEmpty()) {
			for (String key : contextArgs.keySet()) {
				logger.info("key : {} - value : {}", key, contextArgs.get(key));
			}	
		}
		
		createJSONBodyTrigger.PagerDuty_Integration_Key = contextArgs.get("PagerDuty_Integration_Key");
		createJSONBodyTrigger.PagerDuty_URL = contextArgs.get("PagerDuty_URL");
		createJSONBodyTrigger.PagerDuty_Header = contextArgs.get("PagerDuty_Header");
		createJSONBodyTrigger.PagerDuty_Header_Data = contextArgs.get("PagerDuty_Header_Data");
		
		createJSONBodyTrigger.Heal_IP = contextArgs.get("Heal_IP");
		createJSONBodyTrigger.Heal_Port = contextArgs.get("Heal_Port");
		createJSONBodyTrigger.healPermalink = contextArgs.get("healPermalink");
		createJSONBodyTrigger.healSignalTimeFormat = contextArgs.get("healSignalTimeFormat");
		
		createJSONBodyTrigger.signalHeader = contextArgs.get("signalHeader");
		createJSONBodyTrigger.clientHeader = contextArgs.get("clientHeader");
		createJSONBodyTrigger.clientUrl = contextArgs.get("clientUrl");
		
		dedupKeyPrefix = contextArgs.get("dedupKeyPrefix");
		
		

		Map<String, Set<String>> mapSubscription = context.getSubscriptions();
		if(mapSubscription!=null && !mapSubscription.isEmpty()) {
			for (String key : mapSubscription.keySet()) {
				logger.info("key : {} - value : {}", key, mapSubscription.get(key));
			}	
		}
		logger.info("logger plugin initialization completed");
	}

	public boolean process(PluginEvent event) throws PluginException {
		
		logger.info("logger Plugin process...");
		Map<String, String> attributes = event.getAttributes();
		if(attributes!=null && !attributes.isEmpty()) {
			for (String key : attributes.keySet()) {
				logger.info("key : {} - value : {}", key, attributes.get(key));
			}
		}
		String eventSourcceName = event.getEventSource().getName();
		logger.info("event source name : {}", eventSourcceName);
		String eventType = event.getEventType();
		logger.info("event type : {}", eventType);
		byte[] notificationOutput = (byte[]) event.getData();
		try {
			NotificationProtos.Notification notification = NotificationProtos.Notification
					.parseFrom(notificationOutput);
			logger.info("parsed notification: {}", notification.toString());
			
			if(notification.getPlaceHoldersMap().get("Signal_Status").equalsIgnoreCase("Open")) {
				try {
					
					createJSONBodyTrigger.createBody(notification);
					
				}catch(Exception e){
					logger.error("Error in createJSONBodyTrigger: {}",e.getMessage());
				}		
				
			
			}else if(notification.getPlaceHoldersMap().get("Signal_Status").equalsIgnoreCase("Closed")) {
				try {
		
					createJSONBodyResolved.createBody(notification);
					
				}catch(Exception e){
					logger.error("Error in createJSONBodyResolved: {}",e.getMessage());
				}								
			}else {				
				try {					
					createJSONBodyResolved.createBody(notification);				
				}catch(Exception e){
					logger.error("Error in createJSONBodyResolved: {}",e.getMessage());
				}
			}
			
			logger.info("----------------------------------------------------------");

		} catch (InvalidProtocolBufferException e) {
			e.printStackTrace();
		}
		logger.info("logger Plugin process complete...");
		return true;
	}
	
	public static String generateRandomString() {
        String uuid = UUID.randomUUID().toString();
        return uuid;
    }

	public void destroy() throws PluginException {

	}

	public PluginStats getStats() throws PluginException {
		return null;
	}

}
