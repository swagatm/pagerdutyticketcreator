package com.appnomic.appsone.plugin.logs;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.appnomic.appsone.common.protbuf.NotificationProtos.Notification;

public class createJSONBodyTrigger {
	
	public static String PagerDuty_Integration_Key = "";
	public static String PagerDuty_URL = "";
	public static String PagerDuty_Header = "";
	public static String PagerDuty_Header_Data = "";
	public static String Heal_Account_Time_Zone = "";
	public static String Heal_IP = "";
	public static String Heal_Port = "";
	public static String healPermalink = "";
	public static String healSignalTimeFormat = "";
	public static String signalHeader = "";
	public static String clientHeader = "";
	public static String clientUrl = "";
	
	@SuppressWarnings("unchecked")
	public static void createBody(Notification notification) {
		Map<String,String> pH = notification.getPlaceHoldersMap();
		Heal_Account_Time_Zone = pH.get("Timezone").replaceAll("\\s", "");
		JSONObject payloadObj = new JSONObject();
	    payloadObj.put("summary",pH.get("Signal_Description"));
	    payloadObj.put("source",pH.get("Affected_ServiceNames"));
	    
	    if(pH.get("Severity").equalsIgnoreCase("Severe")) {
	    	payloadObj.put("severity","critical");
	    }else if(pH.get("Severity").equalsIgnoreCase("Default")) {
	    	payloadObj.put("severity","warning");
	    }else {
	    	payloadObj.put("severity","info");
	    }	  
	    if(healSignalTimeFormat.equalsIgnoreCase("ms")) {
	    	payloadObj.put("timestamp",timeConversionfromMS(pH.get("StartTime")));
	    }else {
	    	payloadObj.put("timestamp",timeConversionfromString(pH.get("StartTime")));
	    }
	    
	    
	    payloadObj.put("component",pH.get("RootCause_ServiceNames"));
	    payloadObj.put("group",pH.get("Affected_ApplicationNames"));	    
	    String className = classMaker(pH.get("Latest_Events_Detected"));
	    payloadObj.put("class",className);	    
	    payloadObj.put("custom_details",pH.toString());	 	  
	    JSONArray linkArr = new JSONArray();
	    JSONObject linkObj = new JSONObject();	    
	    
	    linkObj.put("href", createPermalink(pH));	    
	    linkObj.put("text", signalHeader);	    
	    linkArr.add(linkObj);
	    JSONObject mainObj = new JSONObject();	    
	    mainObj.put("routing_key",PagerDuty_Integration_Key);
	    
	    mainObj.put("dedup_key", PluginLogs.dedupKeyPrefix+""+pH.get("Signal_ID"));
	    mainObj.put("event_action","trigger");
	    
	    mainObj.put("client",clientHeader);
	    String clientUrl = makeclientUrl();
	    mainObj.put("client_url",clientUrl);
	    

//	    JSONArray imgArr = new JSONArray();
//	    JSONObject imgObj = new JSONObject();
//	    imgObj.put("src","./logo.png");
//	    imgObj.put("href",makeclientUrl());
//	    imgObj.put("alt","Image not available");
//	    imgArr.add(imgObj);
//	    mainObj.put("images",imgArr);
	    
	    mainObj.put("payload",payloadObj);
	    mainObj.put("links",linkArr);
	    String bodyforPagerDuty = mainObj.toJSONString();
	    //System.out.println(bodyforPagerDuty);
	    RestCalltoPagerDuty.sendBodytoPagerDuty(bodyforPagerDuty,PagerDuty_URL,PagerDuty_Header,PagerDuty_Header_Data);
	}
	
	private static String makeclientUrl() {
		// TODO Auto-generated method stub
		String ipSample = "{Heal_IP}";
	    String portSample = "{Heal_Port}";
		clientUrl = clientUrl.replace(ipSample, Heal_IP);
		clientUrl = clientUrl.replace(portSample, Heal_Port);
//		String clientUrl = "https://"+Heal_IP+":"+Heal_Port+"/appsone-ui-service";
		return clientUrl;
	}
	private static String classMaker(String classNameSample) {
		// TODO Auto-generated method stub
		classNameSample = classNameSample.replaceAll("<.*?>", ",");
		classNameSample = classNameSample.replaceAll("^,,|,,$", "");
        String[] arr = classNameSample.split(",,");
        Map<String,String> m = new HashMap<>();
        for(int i =0;i<arr.length;i++){
            String[] temp = arr[i].split(":");
            m.put(temp[0].trim(),temp[1].trim());
        }
        String className = "Host Address: "+m.get("Host address")+" | KPI Name: "+m.get("KPI name")+" | Value: "+m.get("Value");
		return className;
	}
	private static String createPermalink(Map<String, String> pH) {
		// TODO Auto-generated method stub
		String ipSample = "{Heal_IP}";
	    String portSample = "{Heal_Port}";
	    String accountidSample = "{Account_Identifier}";
	    String signalidSample = "{Signal_ID}";
	    String HealSignalLink = healPermalink.replace(ipSample, Heal_IP).replace(portSample, Heal_Port).
	    		replace(accountidSample, pH.get("AccountIdentifier")).replace(signalidSample, pH.get("Signal_ID"));
	    return HealSignalLink;
	}
	public static String timeConversionfromMS(String timeinMS) {
        Date date = new Date(Long.parseLong(timeinMS));
// Conversion
        SimpleDateFormat sdf;
        sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
        sdf.setTimeZone(TimeZone.getTimeZone(Heal_Account_Time_Zone));
        String text = sdf.format(date);
        return text;
	}
	public static String timeConversionfromString(String timeinString) {
		Date temp = null;
		try {
			temp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
			        .parse(timeinString);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	            SimpleDateFormat sdf;
	    sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
	    sdf.setTimeZone(TimeZone.getTimeZone(Heal_Account_Time_Zone));
	    String text = sdf.format(temp);
	    return text;
	}
}
