package com.appnomic.appsone.plugin.logs;

import org.json.simple.JSONObject;

import com.appnomic.appsone.common.protbuf.NotificationProtos.Notification;

public class createJSONBodyResolved {
	public static String PagerDuty_Integration_Key = createJSONBodyTrigger.PagerDuty_Integration_Key;
	public static String PagerDuty_URL = createJSONBodyTrigger.PagerDuty_URL;
	public static String PagerDuty_Header = createJSONBodyTrigger.PagerDuty_Header;
	public static String PagerDuty_Header_Data = createJSONBodyTrigger.PagerDuty_Header_Data;
	@SuppressWarnings("unchecked")
	public static void createBody(Notification notification) {
		// TODO Auto-generated method stub
		JSONObject resolveBody = new JSONObject();
		String uuid = notification.getPlaceHoldersMap().get("Signal_ID");
		resolveBody.put("routing_key", PagerDuty_Integration_Key);
		resolveBody.put("dedup_key", PluginLogs.dedupKeyPrefix+""+uuid);
		resolveBody.put("event_action", "resolve");		
		RestCalltoPagerDuty.sendBodytoPagerDuty(resolveBody.toJSONString(), PagerDuty_URL, PagerDuty_Header, PagerDuty_Header_Data);
	}

}
