package com.appnomic.appsone.plugin.logs;

import java.io.IOException;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class RestCalltoPagerDuty {
	private static Logger logger = LoggerFactory.getLogger(RestCalltoPagerDuty.class);
	public static void sendBodytoPagerDuty(String bodyforPagerDuty, String pagerDuty_URL, String pagerDuty_Header, String pagerDuty_Header_Data) {
		logger.info(bodyforPagerDuty);
			OkHttpClient client = new OkHttpClient().newBuilder()
	                .build();
	        MediaType mediaType = MediaType.parse("application/json");
	       
			RequestBody body = RequestBody.create(mediaType,bodyforPagerDuty);
	        Request request = new Request.Builder()
	                .url(pagerDuty_URL)
	                .method("POST", body)
	                .addHeader(pagerDuty_Header, pagerDuty_Header_Data)
	                .build();
	        try {
	            Response response = client.newCall(request).execute();
	            logger.info(Objects.requireNonNull(response.body()).string());
	        } catch (IOException e) {
	        	logger.error("Error in REST Call:{}\n{}",e.getMessage(),e);
		}
	}
}
